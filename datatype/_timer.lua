--- @class timer
--- @field public OriginalValue number Original value of the timer
--- @field public Value number Current value of the timer
--- @field public ToString string Same as Value