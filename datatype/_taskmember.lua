--- @class taskmember
--- @field public Name string Returns name of the member
--- @field public Leader boolean Returns true if the member is leader
--- @field public Index number Returns task index for member (i.e. 1-6)