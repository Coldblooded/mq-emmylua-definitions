--- @class time
--- @field public Date string Date in the format MM/DD/YYYY
--- @field public Day number Day of the month
--- @field public DayOfWeek number Day of the week (1=sunday to 7=saturday)
--- @field public Hour number Hour (0-23)
--- @field public Minute number Minute (0-59)
--- @field public Month number Month of the year (1-12)
--- @field public Night boolean Gives true if the current hour is considered "night" in EQ (7:00pm-6:59am)
--- @field public Second number Second (0-59)
--- @field public SecondsSinceMidnight number Number of seconds since midnight
--- @field public Time12 string Time in 12-hour format (HH:MM:SS)
--- @field public Time24 string Time in 24-hour format (HH:MM:SS)
--- @field public Year number Year
--- @field public ToString string Same as Time24